/* global Swiper */

'use strict';

$(document).ready(function() {
	// Menu
	function addMobMenu() {
		var mobMenu = $('#mob-menu');
		var menu = mobMenu.next('.menu-wrap');
		var btn = menu.find('.menu__item');
		
		mobMenu.on('click', function(e) {
			e.preventDefault();

			$(this).toggleClass('mob-menu_active');
			menu.toggleClass('menu-wrap_active');

			btn.on('click', function() {
				mobMenu.removeClass('mob-menu_active');
				menu.removeClass('menu-wrap_active');
			});
		});
	}
	addMobMenu();

	// Cookies
	function cookies() {
		var cookies = $('.cookies');
		var btn = cookies.find('.btn');
		var data = sessionStorage.getItem('cookie');
		
		if (data === null) {
			cookies.addClass('js-show');
		}

		btn.on('click', function(e) {
			e.preventDefault();
			cookies.removeClass('js-show');
			sessionStorage.setItem('cookie', 'ok');
		});
	}
	cookies();
	
	// Smooth scroll
	function smoothScroll() {
		var header = $('#header');
		var link = header.find('.menu__item_link');	
		var section = $('.section');
		var logo = header.find('.logo');
		var sectionPrize = $('#prize');

		link.on('click', function() {
			var target = $(this).attr('href');
			var dist = $(target).offset().top - 50;

			$('html, body').stop().animate({
				scrollTop: dist
			}, 700);
			return false;
		});

		$(window).on('scroll', function() {
			var scrollDistance = $(window).scrollTop() + 50;
			var posPrize  = sectionPrize.offset().top - 50;

			if (posPrize > scrollDistance) {
				header.find('.menu__item_link.active').removeClass('active');
			}

			for (let i = 0; i < section.length; i++) {
				var position = section[i].offsetTop - 50;
			
				if (position <= scrollDistance) {
					header.find('.menu__item_link.active').removeClass('active');
					header.find('.menu__item_link').eq(i).addClass('active');
				}
			}
		});

		logo.on('click', function() {
			$('body, html').stop().animate({
				scrollTop: 0
			}, 700);
			return false;
		});
	}
	smoothScroll();

	// PopUp
	function popUp() {
		$('.js-popup-button').on('click', function() {
			$('.popup').removeClass('js-popup-show');
			var popupClass = '.' + $(this).attr('data-popupShow');
			$(popupClass).addClass('js-popup-show');
			if ($(document).height() > $(window).height()) {
				var scrollTop = ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop();
				$('html').addClass('noscroll').css('top', -scrollTop);
			}
		});
		closePopup();
	}

	// Close PopUp
	function closePopup() {
		$('.js-close-popup').on('click', function() {
			$('.popup').removeClass('js-popup-show');
			var scrollTop = parseInt($('html').css('top'));
			$('html').removeClass('noscroll');
			$('html, body').scrollTop(-scrollTop);
			return false;
		});
		
		$('.popup').on('click', function(e) {
			var div = $('.popup__wrap');
			
			if (!div.is(e.target) && div.has(e.target).length === 0) {
				$('.popup').removeClass('js-popup-show');
				var scrollTop = parseInt($('html').css('top'));
				$('html').removeClass('noscroll');
				$('html, body').scrollTop(-scrollTop);
			}
		});
	}
	popUp();
	
	// Swipper
	function swiperInit() {
		var wrap = $('.product-slide-wrap');

		for (let j = 0; j < wrap.length; j++) {
			new Swiper(wrap[j], {
				slidesPerView: 3,
				setWrapperSize: true,
				speed: 400,
				centerInsufficientSlides: true,
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev'
				},
				pagination: {
					el: '.swiper-pagination',
					type: 'bullets',
					clickable: true
				},
				breakpoints: {
					768: {
						slidesPerView: 2,
						slidesPerGroup: 2,
					},
					480: {
						slidesPerView: 1,
						slidesPerGroup: 1,
					}
				}
			});
		} 
	}
	swiperInit();

	// Input type digits
	var inputDigits = document.getElementById('digits');

	inputDigits.onkeypress = function(e) {
		e = e || event;
		var chr = getChar(e);

		if (e.ctrlKey || e.altKey || e.metaKey) return;

		if (chr == null) return;

		if (chr < '0' || chr > '9') {
			return false;
		}
	};

	function getChar(event) {
		if (event.which == null) {
			if (event.keyCode < 32) return null;
			return String.fromCharCode(event.keyCode);
		}

		if (event.which != 0 && event.charCode != 0) {
			if (event.which < 32) return null;
			return String.fromCharCode(event.which);
		}

		return null;
	}

	// Scroll
	$('#winner-wrap-table').scrollbar();

});